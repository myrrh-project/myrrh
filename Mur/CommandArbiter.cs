﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Serilog;

using Myrrh.Commands;
using Myrrh.Events;
using Myrrh.Values;


namespace Myrrh
{
    class CommandArbiter
    {
        private Dictionary<int, Dictionary<string, AKeyValue>> Data;
        private ILogger Logger;
        private UnixSocketListener Socket;

        public CommandArbiter(ILogger logger, UnixSocketListener socket)
        {
            Logger = logger;
            Socket = socket;
            socket.RaiseTextReceived += HandleTextReceived;
            Data = new Dictionary<int, Dictionary<string, AKeyValue>>();
        }

        public void HandleTextReceived(object sender, TextReceivedArgs e)
        {
            ICommand c = null;
            var pair = Resp.decode(e.text);
            string[] vs = null;
            if(pair.Item1.GetType() == typeof(object[]))
            {
                var castedPair = (object[])pair.Item1;
                vs = new string[castedPair.Length];
                for(var i = 0; i < vs.Length; i++)
                {
                    if(castedPair[i].GetType() == typeof(string))
                    {
vs[i] = (string)castedPair[i];
                    }

                    
                    Console.WriteLine(vs[i]);
                }
                if(vs[0] == "GET")
                {
                    c = new GetCommand();
                                    } else if(vs[0] == "SET")
                {
                    c = new SetCommand();
                }
            }


            if (c != null && vs != null)
            {
                if(Data.ContainsKey(e.uid) == false)
                {
                    Data.Add(e.uid, new Dictionary<string, AKeyValue>());
                }

                var userDict = Data[e.uid];
                var result = c.Execute(vs, ref userDict);
                Data[e.uid] = userDict;
                Socket.SocketWrite(e.socket, result.GetResponseByteArray());

            } else
            {
                Socket.SocketWrite(e.socket, (new Myrrh.Responses.ErrorResponse("ERR")).GetResponseByteArray());
            }
        }
        private string bulkString() => String.Empty;
    }


    /// <summary>
    /// https://xmonader.github.io/nimdays/day12_resp.html
    /// </summary>
    public class Resp
    {
        private uint idx;
        private string line;

        private static Tuple<object, int> decodeArray(string s)
        {
            object[] arr;
            var ctrlPos = s.IndexOf("\r\n");
            var arrLenStr = s.Substring(1, ctrlPos - 1);

            var arrLen = int.Parse(arrLenStr);

            var i = s.IndexOf("\r\n") + "\r\n".Length;

            if(arrLen == -1)
            {
                return new Tuple<object, int>(new string[0], i);
            }

            arr = new object[arrLen];

            var arrIdx = 0;
            while (i < s.Length && arrIdx < arrLen)
            {
                var pair = decode(s.Substring(i));
                var obj = pair.Item1;
                i += pair.Item2;
                arr[arrIdx] = obj;
                arrIdx++;
            }

            return new Tuple<object, int>(arr, i + "\r\n".Length);

        }

        public static Tuple<object, int> decode(string s)
        {
            var i = 0;
            while (i < s.Length)
            {
                var curChar = s[i];
                if (curChar == '+')
                {
                    var pair = decodeStr(s.Substring(i, s.IndexOf("\r\n") + "\r\n".Length));
                    var obj = pair.Item1;
                    var count = pair.Item2;
                    i += count;
                    return new Tuple<object, int>(obj, i);
                }
                else if (curChar == '-')
                {
                    var pair = decodeError(s.Substring(i, s.IndexOf("\r\n") + "\r\n".Length));
                    var obj = pair.Item1;
                    var count = pair.Item2;
                    i += count;
                    return new Tuple<object, int>(obj, i);
                }
                else if (curChar == '$')
                {
                    var pair = decodeBulkStr(s.Substring(i));
                    var obj = pair.Item1;
                    var count = pair.Item2;
                    i += count;
                    return new Tuple<object, int>(obj, i);
                }
                else if (curChar == ':')
                {
                    var pair = decodeInt(s.Substring(i, s.IndexOf("\r\n") + "\r\n".Length));
                    var obj = pair.Item1;
                    var count = pair.Item2;
                    i += count;
                    return new Tuple<object, int>(obj, i);
                }
                else if (curChar == '*')
                {
                    var pair = decodeArray(s.Substring(i));
                    var obj = pair.Item1;
                    var count = pair.Item2;
                    i += count;
                    return new Tuple<object, int>(obj, i);
                }
                else
                {
                    // log, eventually
                }
                i++;
                
            }
            return new Tuple<object, int>(null, i);
        }

        private static Tuple<object, int> decodeBulkStr(string s)
        {
            var crlfpos = s.IndexOf("\r\n");
            
            var sLen = s.Substring(1, crlfpos - 1);
            var bulkLen = int.Parse(sLen);
            string bulk;
            if(bulkLen == -1)
            {
                bulk = null;
                return new Tuple<object, int>(null, crlfpos + "\r\n".Length);
            } else
            {
                var i = crlfpos + "\r\n".Length;
                bulk = s.Substring(i, bulkLen);
                i += bulkLen;
                return new Tuple<object, int>(bulk, i + "\r\n".Length);
            }

        }

        private static Tuple<object, int> decodeInt(string s)
        {
            int i;
            var crlfpos = s.IndexOf("\r\n");
            var sInt = s.Substring(1, crlfpos - 1);
            i = int.Parse(sInt);
            return new Tuple<object, int>(i, crlfpos + "\r\n".Length);
        }

        private static Tuple<object, int> decodeError(string s)
        {
            var crlfpos = s.IndexOf("\r\n");
            return new Tuple<object, int>(s.Substring(1, crlfpos - 1), crlfpos + "\r\n".Length);
        }

        private static Tuple<object, int> decodeStr(string s)
        {
            var crlfpos = s.IndexOf("\r\n");
            return new Tuple<object, int>(s.Substring(1, crlfpos - 1), crlfpos + "\r\n".Length);
        }
    }
}
