﻿using System;
using System.Collections.Generic;
using System.Text;

using Myrrh.Responses;
using Myrrh.Values;

namespace Myrrh.Commands
{
    class GetCommand : ICommand
    {
        public IResponse Execute(string[] args, ref Dictionary<string, AKeyValue> dic)
        {
            AKeyValue value;

            var key = args[1];

            if (dic.TryGetValue(key, out value))
            {
                Console.WriteLine(value);
                return new StringResponse(value.ToString());
            } else
            {
                Console.WriteLine("key not found");
                return new NullResponse();
            }

        }
    }
}
