﻿using System.Collections.Generic;

using Myrrh.Responses;
using Myrrh.Values;

namespace Myrrh.Commands
{
    struct CommandResult
    {
        public bool success;
        public object value;
    }

    interface ICommand
    {
        public IResponse Execute(string[] args, ref Dictionary<string, AKeyValue> dic);
    }
}
