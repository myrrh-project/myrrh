﻿using System;
using System.Collections.Generic;
using System.Text;

using Myrrh.Responses;
using Myrrh.Values;

namespace Myrrh.Commands
{
    class IncrCommand : ICommand
    {
        public IResponse Execute(string[] args, ref Dictionary<string, AKeyValue> dic)
        {
            var key = args[0];
            if (dic.ContainsKey(args[0]) == false)
            {
                dic.Add(key, new IntValue(1));
                return new IntResponse(1);
            }

            if(dic[key] is IntValue)
            {
                IntValue currentValue = (IntValue)dic[key];
                currentValue.Incr();
            } else if(dic[key] is StringValue)
            {
                int valueInt;
                StringValue currentValue = (StringValue)dic[key];
                if(int.TryParse(currentValue.ToString(), out valueInt))
                {
                    valueInt++;
                    IntValue newValue = new IntValue(valueInt);
                    dic[key] = newValue;

                    return new IntResponse(valueInt);
                } else
                {
                    return new ErrorResponse("ERR value is not an integer or out of range");
                }
            }

            return new ErrorResponse("ERR value is not an integer or out of range");
        }
    }
}
