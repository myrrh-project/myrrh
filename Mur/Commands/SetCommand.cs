﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

using Myrrh.Responses;
using Myrrh.Values;

namespace Myrrh.Commands
{
    class SetCommand : ICommand
    {
        public IResponse Execute(string[] args, ref Dictionary<string, AKeyValue> dic)
        {
            Console.WriteLine("executing set command");
            if (args.Length < 3)
            {
                throw new ArgumentException("args must contain at least three strings!");
            }


            string[] a = new string[(args.Length - 2)];
            Array.Copy(args, 2, a, 0, args.Length - 2);
            var key = args[1];
            string str = String.Join(" ", a);

            

            Regex rx = new Regex("\".*?\"");
            var match = rx.Match(String.Join(" ", a));


            if (match.Success || true)
            {

                var value = new StringValue(match.Value);
                value = new StringValue(args[2]);
                //var value = new StringValue(str);

                Console.WriteLine("Key : " + args[1] + " Value: " + value);

                if (dic.ContainsKey(key))
                {
                    dic[key] = value;// hashtable.Remove(args[1]);
                }
                else
                {
                    dic.Add(key, value);
                }

                Console.WriteLine(dic.Count);
                return new StringResponse("OK");
            }
            else
            {

                return new NullResponse();
            }

        }
    }
}
