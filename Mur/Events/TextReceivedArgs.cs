﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Events
{
    class TextReceivedArgs : EventArgs
    {
        public int socket = -1;
        public byte[] bytes = new byte[] { };
        public string text = String.Empty;
        public int pid = -1;
        public int uid = -1;
        public int gid = -1;
    }
}
