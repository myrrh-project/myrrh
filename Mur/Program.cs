﻿using System;
using System.IO;

using Serilog;

namespace Myrrh
{
    class MainClass
    {

        public static void Main(string[] args)
        {
            var logger = new LoggerConfiguration()
                .WriteTo.Debug()
                .CreateLogger();

            if (args.Length == 0)
            {
                logger.Fatal("No path argument sent.");
                return;
            }

            var path = args[0];
            if(File.Exists(path))
            {
                File.Delete(path);
            }

            var listener = new UnixSocketListener(logger, path);
            var ca = new CommandArbiter(logger, listener);
            listener.Start();
            return;
        }
    }
}
