﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Responses
{
    class ErrorResponse : IResponse
    {
        private readonly string Error;

        public ErrorResponse(string error)
        {
            Error = error;
        }

        public byte[] GetResponseByteArray()
        {
            return System.Text.Encoding.UTF8.GetBytes("-" + Error + "\r\n");
        }
    }
}
