﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Responses
{
    interface IResponse
    {
        public byte[] GetResponseByteArray();
    }
}
