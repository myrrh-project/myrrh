﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Responses
{
    class IntResponse : IResponse
    {
        private int Value;

        public IntResponse(int value)
        {
            Value = value;
        }

        public void Incr()
        {
            Value += 1;
        }

        public void Decr()
        {
            Value -= 1;
        }

        byte[] IResponse.GetResponseByteArray()
        {
            return System.Text.Encoding.UTF8.GetBytes(":" + Value.ToString() + "\r\n");
        }
    }
}
