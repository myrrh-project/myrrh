﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Responses
{
    class NullResponse : IResponse
    {
        public byte[] GetResponseByteArray() => new byte[] { 0x24, 0x2D, 0x31, 0x0D, 0x0A };
    }
}
