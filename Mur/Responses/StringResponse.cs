﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Responses
{
    class StringResponse : IResponse
    {
        private readonly string Response;

        public StringResponse(string response)
        {
            Response = response;
        }

        public byte[] GetResponseByteArray()
        {
            string resp = @"$" + Response.Length.ToString() + "\r\n" + Response + "\r\n";
            return System.Text.Encoding.UTF8.GetBytes(resp);
        }
    }
}
