﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using Mono.Unix;
using Mono.Unix.Native;
using Serilog;

using Myrrh.Exceptions;
using Myrrh.Events;
using Myrrh.ThirdParty.StackOverflow;

namespace Myrrh
{
    class UnixSocketListener
    {
        private List<Pollfd> Fds = new List<Pollfd>();
        private ILogger Logger;
        private string Path;
        private bool Listening;
        
        public event EventHandler<TextReceivedArgs> RaiseTextReceived;

        public UnixSocketListener(ILogger logger, string path)
        {
            Logger = logger;
            Path = path;
        }

        public void Start()
        {
            if(Listening == true)
            {
                throw new Exception("Already Listening!");
            }

            Listening = true;
            var listenerThread = new Thread(() => DoListen())
            {
                IsBackground = false
            };
            
            listenerThread.Start();
        }

        public void Stop()
        {
            Listening = false;
        }

        private unsafe void DoListen()
        {
            if (File.Exists(Path))
            {
                throw new SocketExistsException();
            }

            var socketPath = new SockaddrUn(Path);
            var listenerSocket = Syscall.socket(UnixAddressFamily.AF_UNIX, UnixSocketType.SOCK_STREAM, (UnixSocketProtocol)0);
            Syscall.setsockopt(listenerSocket, UnixSocketProtocol.SOL_SOCKET, UnixSocketOptionName.SO_REUSEADDR, 1);
            Syscall.setsockopt(listenerSocket, UnixSocketProtocol.SOL_SOCKET, UnixSocketOptionName.SO_PASSCRED, 1);
            Syscall.bind(listenerSocket, socketPath);
            Syscall.listen(listenerSocket, 10);


            addFd(listenerSocket);

Console.WriteLine("Listening...");
            while (Listening)
            {
                var fdsArr = Fds.ToArray();
                int pollRet = Syscall.poll(fdsArr, (uint)fdsArr.Length, -1);

                if(pollRet == -1)
                {
                    throw new Exception("Polling Error");
                }

                for(int i = 0; i < fdsArr.Length; i++)
                {
                    if ((fdsArr[i].revents & PollEvents.POLLIN) == 0)
                    {
                        continue;
                    }

                    if (fdsArr[i].fd == listenerSocket)
                    {
                        var remoteAddrUn = new SockaddrUn();
                        int clientSocket = Syscall.accept(listenerSocket, remoteAddrUn);

                        if (clientSocket == -1)
                        {
                            Logger.Warning("Unable to accept client");
                        }
                        else
                        {
                            addFd(clientSocket);

                        }
                    } else
                    {
                        byte[] cmsgBuffer = new byte[1024];
                        byte[] dataBuffer = new byte[1024];
                        byte[] cmsg = new byte[1024];

                        Msghdr msghdr = new Msghdr
                        {
                            msg_control = cmsg,
                            msg_controllen = cmsg.Length,
                        };

                        long ret = 0;
                        fixed (byte* ptr_buffer = cmsgBuffer, ptr_data_buffer = dataBuffer)
                        {
                            Iovec[] iovecs = new Iovec[] {
                                new Iovec {
                                    iov_base = (IntPtr) ptr_buffer,
                                    iov_len = (ulong) cmsgBuffer.Length,
                                },
                            };
                            msghdr.msg_iov = iovecs;
                            msghdr.msg_iovlen = 1;
                            do
                            {
                                ret = Syscall.recvmsg(fdsArr[i].fd, msghdr, (MessageFlags)0);
                            } while (UnixMarshal.ShouldRetrySyscall((int)ret));
                        }

                        if(ret > 0)
                        {
                            var msgBytes = new byte[ret];
                            Array.Copy(cmsgBuffer, msgBytes, ret);
                            var bufferedMessage = System.Text.Encoding.UTF8.GetString(cmsgBuffer, 0, (int)ret).Trim(Environment.NewLine.ToCharArray());


                            var receivedArgs = new TextReceivedArgs()
                            {
                                socket = fdsArr[i].fd,
                                bytes = msgBytes,
                                text = bufferedMessage,
                            };

                            for (long cmsgp = Syscall.CMSG_FIRSTHDR(msghdr);
                                cmsgp != -1;
                                cmsgp = Syscall.CMSG_NXTHDR(msghdr, cmsgp))
                            {
                                var recvHdr = Cmsghdr.ReadFromBuffer(msghdr, cmsgp);
                                if (recvHdr.cmsg_level == UnixSocketProtocol.SOL_SOCKET && recvHdr.cmsg_type == UnixSocketControlMessage.SCM_CREDENTIALS)
                                {
                                    var recvDataOffset = Syscall.CMSG_DATA(msghdr, cmsgp);
                                    var sizeOfHdrbytes = recvHdr.cmsg_len - (recvDataOffset - cmsgp);
                                    if (sizeOfHdrbytes / sizeof(int) != 3)
                                    {
                                        Logger.Error(new Exception("Incorrect size of cmsghdr"), "Incorrect size of cmsghdr");
                                        throw new Exception("Incorrect size of cmshdr");
                                    }

                                    fixed (byte* ptr = msghdr.msg_control)
                                    {
                                        receivedArgs.pid = ((int*)(ptr + recvDataOffset))[0];
                                        receivedArgs.uid = ((int*)(ptr + recvDataOffset))[1];
                                        receivedArgs.gid = ((int*)(ptr + recvDataOffset))[2];
                                    }
                                }
                            }

                            if ((msghdr.msg_flags & MessageFlags.MSG_CTRUNC) != 0)
                                throw new Exception("Control message truncated (probably file descriptors lost)");

                            if (receivedArgs.pid == -1)
                            {
                                Logger.Error("Unable to receive process ID from cmsg!");
                            }
                            
                            DoRaiseTextReceived(receivedArgs);
                            
                        } else
                        {
                            closeAndRemoveFd(fdsArr[i]);
                        }
                    }
                }
            }

            File.Delete(Path);
        }

        public void SocketWrite(int fd, byte[] byteArray)
        {
            Syscall.send(fd, byteArray, (ulong)byteArray.Length, (MessageFlags)0);
        }

        private void addFd(int socket)
        {
            Fds.Add(new Pollfd
            {
                fd = socket,
                events = PollEvents.POLLIN,
            });
        }

        private void closeAndRemoveFd(Pollfd fdsArr)
        {
            Syscall.close(fdsArr.fd);
            Fds.Remove(fdsArr);
        }

        private void DoRaiseTextReceived(TextReceivedArgs e)
        {
            EventHandler<TextReceivedArgs> eventHandler = RaiseTextReceived;

            if(eventHandler != null)
            {
                eventHandler(this, e);
            }
        }
    }
}
