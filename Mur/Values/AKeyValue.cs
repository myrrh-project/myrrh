﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Values
{
    abstract class AKeyValue
    {
        private readonly long createdAt = DateTimeOffset.Now.ToUnixTimeSeconds();
        private int ttl = -1;

        public void SetTTL(int ttl) => this.ttl = ttl;
        /* {
             this.hasttl = true;

         }
        */

        public bool IsExpired() => (this.ttl > 0) && (DateTimeOffset.Now.ToUnixTimeSeconds() >= (this.createdAt + this.ttl));
            /*
        {
            return ;
        }
            */
        public override abstract string ToString();
    }
}
