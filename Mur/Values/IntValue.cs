﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Values
{
    class IntValue : AKeyValue
    {
        private int Value;

        public IntValue(int value)
        {
            Value = value;
        }

        public void Incr()
        {
            Value += 1;
        }

        public void Decr()
        {
            Value -= 1;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
