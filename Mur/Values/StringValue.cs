﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Myrrh.Values
{
    class StringValue : AKeyValue
    {
        private string Value;

        public StringValue(string value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
